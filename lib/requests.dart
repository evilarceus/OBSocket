import 'package:obsocket/OBSWebsocket.dart';
import 'dart:async';
import 'package:crypto/crypto.dart';
import 'dart:convert';

class General {
  static Future<dynamic> getVersion() async {
    var completer = new Completer();
    OBSWebsocket.send("GetVersion").then((response) {
      completer.complete(response);
    });
    return completer.future;
  }

  static Future<dynamic> getAuthRequired() async {
    var completer = new Completer();
    OBSWebsocket.send("GetAuthRequired").then((response) {
      completer.complete(response);
    });
    return completer.future;
  }

  static Future<bool> authenticate(String auth) async {
    Completer<bool> completer = new Completer();
    getAuthRequired().then((response) {
      String secret = Util.hashEncode(auth + response["salt"]);
      String authResponse = Util.hashEncode(secret + response["challenge"]);
      OBSWebsocket.send("Authenticate", {"auth": authResponse}).then((success) {
        completer.complete(true);
      }).catchError((e) => completer.complete(false));
    });
    return completer.future;
  }

  static Future<dynamic> setHeartbeat(bool enable) async {
    Completer completer = new Completer();
    OBSWebsocket.send("SetHeartbeat", {"enable": enable}).then((response) {
      completer.complete((response));
    });
    return completer.future;
  }

  static Future<dynamic> setFilenameFormatting(String filenameFormatting) async {
    Completer completer = new Completer();
    OBSWebsocket.send("SetFilenameFormatting", {"filename-formatting": filenameFormatting}).then((response) {
      completer.complete(response);
    });
    return completer.future;
  }

  static Future<String> getFilenameFormatting() async {
    Completer completer = new Completer();
    OBSWebsocket.send("GetFilenameFomatting").then((response) {
      completer.complete(response["filename-formatting"]);
    });
    return completer.future;
  }
}

class Profiles {
  static Future<dynamic> setCurrentProfile(String profileName) async {
    Completer completer = new Completer();
    OBSWebsocket.send("SetCurrentProfile", {"profile-name": profileName}).then((response) {
      completer.complete(response);
    });
    return completer.future;
  }

  static Future<String> getCurrentProfile() async {
    Completer completer = new Completer();
    OBSWebsocket.send("GetCurrentProfile").then((response) {
      completer.complete(response["profile-name"]);
    });
    return completer.future;
  }

  static Future<List> listProfiles() async {
    Completer completer = new Completer();
    OBSWebsocket.send("ListProfiles").then((response) {
      completer.complete(response["profiles"]);
    });
    return completer.future;
  }
}

class Recording {
  static Future<dynamic> startStopRecording() async {
    Completer completer = new Completer();
    OBSWebsocket.send("StartStopRecording").then((response) {
      completer.complete(response);
    });
    return completer.future;
  }

  static Future<bool> startRecording() async {
    Completer completer = new Completer();
    OBSWebsocket.send("StartRecording").then((response) {
      completer.complete(true);
    }).catchError(() => completer.completeError(false));
    return completer.future;
  }

  static Future<bool> stopRecording() async {
    Completer completer = new Completer();
    OBSWebsocket.send("StopRecording").then((response) {
      completer.complete(true);
    }).catchError(() => completer.completeError(false));
    return completer.future;
  }

  static Future<dynamic> setRecordingFolder(String recFolder) async {
    Completer completer = new Completer();
    OBSWebsocket.send("SetRecordingFolder", {"recFolder": recFolder}).then((response) {
      completer.complete(response);
    });
    return completer.future;
  }

  static Future<String> getRecordingFolder() async {
    Completer completer = new Completer();
    OBSWebsocket.send("GetRecordingFolder").then((response) {
      completer.complete(response);
    });
    return completer.future;
  }
}

class ReplayBuffer {
  static Future<dynamic> startStopReplayBuffer() async {
    Completer completer = new Completer();
    OBSWebsocket.send("StartStopReplayBuffer").then((response) {
      completer.complete(response);
    });
    return completer.future;
  }

  static Future<bool> startReplayBuffer() async {
    Completer completer = new Completer();
    OBSWebsocket.send("StartReplayBuffer").then((response) {
      completer.complete(true);
    }).catchError(() => completer.completeError(false));
    return completer.future;
  }

  static Future<bool> stopReplayBuffer() async {
    Completer completer = new Completer();
    OBSWebsocket.send("StopReplayBuffer").then((response) {
      completer.complete(true);
    }).catchError(() => completer.completeError(false));
    return completer.future;
  }

  static Future<bool> saveReplayBuffer() async {
    Completer completer = new Completer();
    OBSWebsocket.send("SaveReplayBuffer").then((response) {
      completer.complete(true);
    }).catchError(() => completer.completeError(false));
    return completer.future;
  }
}

class SceneCollections {
  static Future<dynamic> setCurrentSceneCollection(String scName) async {
    Completer completer = new Completer();
    OBSWebsocket.send("SetCurrentSceneCollection", {"sc-name": scName}).then((response) {
      completer.complete(response);
    });
    return completer.future;
  }

  static Future<dynamic> getCurrentSceneCollection() async {
    Completer completer = new Completer();
    OBSWebsocket.send("GetCurrentSceneCollection").then((response) {
      completer.complete(response["sc-name"]);
    });
    return completer.future;
  }

  static Future<List> listSceneCollections(String scName) async {
    Completer completer = new Completer();
    OBSWebsocket.send("ListSceneCollections").then((response) {
      completer.complete(response["scene-collections"]);
    });
    return completer.future;
  }
}

class SceneItems {
  static Future<dynamic> getSceneItemProperties(String itemId, String itemName, [String scene]) async {
    Completer completer = new Completer();
    OBSWebsocket.send("GetSceneItemProperties", {"item.id": itemId, "item.name": itemName, "scene": scene}).then((response) {
      completer.complete(response);
    });
    return completer.future;
  }

  static Future<dynamic> setSceneItemProperties([
    String scene,
    String itemName, int itemId,
    int itemPositionX, int itemPositionY, int itemPositionAlignment,
    double itemRotation, double itemScaleX, double itemScaleY, // i know what you're thinking and i don't like it either
    int itemCropTop, int itemCropBottom, int itemCropLeft, int itemCropRight,
    bool itemVisible, bool itemLocked,
    String itemBoundsType, int itemBoundsAlignment, double itemBoundsX, double itemBoundsY]) async {
    // TODO: Implement this function
  }

  static Future<dynamic> resetSceneItem(String item, [String sceneName]) async {
    Completer completer = new Completer();
    OBSWebsocket.send("ResetSceneItem", {"item": item, "scene-name": sceneName}).then((response) {
      completer.complete(response);
    });
    return completer.future;
  }

  static Future<dynamic> setSceneItemRender(String source, bool render, [String sceneName]) async {
    Completer completer = new Completer();
    OBSWebsocket.send("ResetSceneItem", {"source": source, "render": render, "scene-name": sceneName}).then((response) {
      completer.complete(response);
    });
    return completer.future;
  }

  @Deprecated("Prefer the use of SetSceneItemProperties")
  static Future<dynamic> setSceneItemPosition(String item, double x, double y, [String sceneName]) async {
    Completer completer = new Completer();
    OBSWebsocket.send("SetSceneItemPosition", {"scene-name": sceneName, "item": item, "x": x, "y": y}).then((response) {
      completer.complete(response);
    });
    return completer.future;
  }

  @Deprecated("Prefer the use of SetSceneItemProperties")
  static Future<dynamic> setSceneItemTransform(String item, double xScale, double yScale, double rotation, [String sceneName]) async {
    Completer completer = new Completer();
    OBSWebsocket.send("SetSceneItemTransform", {"scene-name": sceneName, "item": item, "x-scale": xScale, "y-scale": yScale, "rotation": rotation}).then((response) {
      completer.complete(response);
    });
    return completer.future;
  }


  @Deprecated("Prefer the use of SetSceneItemProperties")
  static Future<dynamic> setSceneItemCrop(String item, int top, int bottom, int left, int right, [String sceneName]) async {
    Completer completer = new Completer();
    OBSWebsocket.send("SetSceneItemTransform", {"scene-name": sceneName, "item": item, "top": top, "bottom": bottom, "left": left, "right": right}).then((response) {
      completer.complete(response);
    });
    return completer.future;
  }
}

class Scenes {
  static Future<dynamic> setCurrentScene(String sceneName) async {
    Completer completer = new Completer();
    OBSWebsocket.send("SetCurrentScene", {"scene-name": sceneName}).then((response) {
      completer.complete(response);
    });
    return completer.future;
  }

  static Future<dynamic> getCurrentScene() async {
    Completer completer = new Completer();
    OBSWebsocket.send("GetCurrentScene").then((response) {
      completer.complete(response);
    });
    return completer.future;
  }

  static Future<dynamic> getSceneList() async {
    Completer completer = new Completer();
    OBSWebsocket.send("GetSceneList").then((response) {
      completer.complete(response);
    });
    return completer.future;
  }

  static Future<dynamic> setSceneItemOrder(List items, [int itemsId, String itemsName, String scene]) async {
    Completer completer = new Completer();
    OBSWebsocket.send("SetSceneItemOrder", {"scene": scene, "items": items, "items[].id": itemsId, "items[].name": itemsName}).then((response) {
      completer.complete(response);
    });
    return completer.future;
  }
}

class Util {
  static String hashEncode(String input) {
    Utf8Encoder utf8 = new Utf8Encoder();
    Base64Encoder base64 = new Base64Encoder();

    var bytes = utf8.convert(input);
    var digest = sha256.convert(bytes);
    return base64.convert(digest.bytes).toString();
  }
}