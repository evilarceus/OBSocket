import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:core';

import 'package:obsocket/requests.dart';

class OBSWebsocket {
  static WebSocket ws;

  static StreamController msgEmitter = new StreamController.broadcast(); // Use this to send messages
  static Stream get msgStream => msgEmitter.stream; // Use this to listen for messages

  /// Connects to OBS WebSocket server
  ///
  /// Args: ip (IP Address), port (Server port, defaults to 4444)
  static Future<dynamic> connect(String ip, [int port = 4444]) async {
    var completer = new Completer();
    OBSWebsocket _obs = new OBSWebsocket();
    WebSocket.connect("ws://" + ip + ":" + port.toString()).then((WebSocket socket) async {
      ws = socket; // Set global variable to socket
      ws.listen((_obs.onMessage)); // Register listener and handler

      General.getAuthRequired().then((res) {
        print(res);
        if (res["authRequired"] == true) {
          completer.completeError("password_required"); // Password required by server
        } else {
          completer.complete(true); // Connection successful
        }
      });
    }).catchError((e) { completer.completeError("connection"); } ); // Connection failed
    return completer.future;
  }

  static Future disconnect() async {
    var completer = new Completer();
    ws.close(1000, "User requested to disconnect").then(completer.complete);
    return completer.future;
  }

  /// Parse message
  void onMessage(dynamic message) async {
    var response = JSON.decode(message); // Convert JSON to Map
    msgEmitter.add(response); // Add decoded response to stream
  }

  /// Send request
  static Future<dynamic> send(String msg, [Map<String, dynamic> data]) {
    var completer = new Completer();
    var req = <String, dynamic>{"request-type": msg, "message-id": "obsocket-" + msg};
    if (data != null) { req.addAll(data); }
    ws.add(JSON.encode(req)); // Send request to obs-websocket
    var resListen;
    resListen = msgStream.listen((response) {
      if (response["message-id"] == "obsocket-" + msg) {
        resListen.cancel();
        if (response["status"] == "error") return completer.completeError(response["status"]);
        completer.complete(response);
      }
    });
    return completer.future;
  }


}
