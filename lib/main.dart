import 'package:flutter/material.dart';
import 'package:obsocket/OBSWebsocket.dart';
import 'dart:async';
import 'requests.dart';

void main() {
  runApp(new MaterialApp(
    home: new OBSHome()
  ));
}

class OBSHome extends StatefulWidget {
  @override
  OBSHomeState createState() => new OBSHomeState();
}



class OBSHomeState extends State<OBSHome> {
  String ip = "";
  String password = "";
  int port = 4444;

  @override
  Widget build(BuildContext context) {
    Future<Null> _dialogTemplate(String title, String content, [Function action]) async {
      if (action == null) action = () { Navigator.of(context).pop(); };
      await showDialog(
          context: context,
          child: new AlertDialog(
            title: new Text(title),
            content: new SingleChildScrollView(
                child: new Text(content)
            ),
            actions: <Widget>[
              new FlatButton(onPressed: action, child: new Text("Ok"))
            ],
          )
      );
    }

    Future<Null> _passwordDialog() async {
      await showDialog(
          context: context,
          child: new AlertDialog(
              title: new Text("Authentication"),
              content: new SingleChildScrollView(
                  child: new TextField(
                    decoration: new InputDecoration(
                        contentPadding: new EdgeInsets.symmetric(vertical: 4.0),
                        hintText: "Password"
                    ),
                    obscureText: true,
                    onChanged: (String text) {
                      setState(() {
                        password = text;
                      });
                    },
                  )
              ),
              actions: <Widget>[
                new FlatButton(onPressed: () { OBSWebsocket.disconnect().then((_) { Navigator.of(context).pop(); } );  }, child: new Text("Cancel")),
                new FlatButton(onPressed: () {
                  Navigator.of(context).pop();
                  General.authenticate(password).then((res) {
                    if (res) {
                      Navigator.push(context, new MaterialPageRoute(builder: (context) => new ControlPanel()));
                    } else {
                      return _dialogTemplate("Password Incorrect", "The password you entered was incorrect. Please try again.", () { Navigator.of(context).pop(); _passwordDialog(); });
                    }
                  });
                } , child: new Text("Submit"))
              ]
          )
      );
    }
    return new Scaffold(
      appBar: new AppBar(title: new Text("OBSocket"), backgroundColor: Colors.green),
      body: new Container(
        padding: new EdgeInsets.symmetric(horizontal: 48.0),
        child: new Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Image.asset("graphics/obs.png", width: 200.0),

              new TextField(
                decoration: new InputDecoration(
                  contentPadding: new EdgeInsets.symmetric(vertical: 6.0),
                  hintText: "IP Address"
                ),
                onChanged: (String text) {
                  setState(() {
                    ip = text;
                  });
                },
                maxLines: 1
              ),
              new TextField(
                  keyboardType: TextInputType.number,
                  decoration: new InputDecoration(
                    contentPadding: new EdgeInsets.symmetric(vertical: 6.0),
                    hintText: "Port (defaults to 4444)",
                  ),
                  onChanged: (String text) {
                    port = int.parse(text);
                  }
              ),
              new Padding(
                padding: new EdgeInsets.all(8.0),
                child: new RaisedButton(
                    child: new Text("CONNECT"),
                    color: Colors.green,
                    onPressed: () {
                      print([ip, port]);
                      OBSWebsocket.connect(ip, port).then((connection) {
                        if (connection == true) {
                          Navigator.push(context, new MaterialPageRoute(builder: (context) => new ControlPanel()));
                        }
                      }).catchError((e) {
                        switch(e) {
                          case "connection":
                            _dialogTemplate("Connection failed", "Could not connect to OBS. Please check your settings and install obs-websocket on the host PC if you haven't already.");
                            break;
                          case "password_required":
                            _passwordDialog();
                            break;
                        }
                      });
                    }
                )
              )
            ]
          )
        )
      )
    );
  }
}

class ControlPanel extends StatefulWidget {
  @override
  ControlPanelState createState() => new ControlPanelState();
}

class ControlPanelState extends State<ControlPanel> {

  PageController _pageController;
  int _page = 0;

  Future<Null> exitDialog() async {
    await showDialog(
        context: context,
        child: new AlertDialog(
          title: new Text("Are you sure?"),
          content: new SingleChildScrollView(
              child: new Text("Are you sure you want to disconnect from OBS?")
          ),
          actions: <Widget>[
            new FlatButton(onPressed: () {
              OBSWebsocket.disconnect().then((res) {
                Navigator.of(context).pop();
                Navigator.of(context).pop(); // i'm pretty sure this is not how you're supposed to properly do this but it works
              });
            }, child: new Text("Yes")),
            new FlatButton(onPressed: () {
              Navigator.of(context).pop();
              }, child: new Text("No"))
          ],
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(child:
    new Scaffold(
      appBar: new AppBar(
          title: new Text("OBSocket"),
          backgroundColor: Colors.green,
          automaticallyImplyLeading: false,
          leading: IconButton(
              icon: Icon(Icons.clear),
              onPressed: () {
                exitDialog(); // Ask user if they want to exit from control panel
              }
          )
      ),
      body: new PageView(
        children: [
          new Container(
            color: Colors.red
          ),
          new Container(
            color: Colors.blue
          ),
          new Container(
            color: Colors.yellow
          )
        ],

        controller: _pageController,
        onPageChanged: onPageChanged,
      ),
      bottomNavigationBar: new BottomNavigationBar(items: [
        new BottomNavigationBarItem(icon: new Icon(Icons.apps), title: new Text("Scenes")),
        new BottomNavigationBarItem(icon: new Icon(Icons.audiotrack), title: new Text("Audio")),
        new BottomNavigationBarItem(icon: new Icon(Icons.folder), title: new Text("Profiles"))
      ],
        onTap: navigationTapped,
        currentIndex: _page,
      ),
    ), onWillPop: () {
      exitDialog();
    });
  }

  void navigationTapped(int page) {
    _pageController.animateToPage(page, duration: const Duration(milliseconds: 300), curve: Curves.ease);
  }

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }

  @override
  void initState() {
    super.initState();
    _pageController = new PageController();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }
}